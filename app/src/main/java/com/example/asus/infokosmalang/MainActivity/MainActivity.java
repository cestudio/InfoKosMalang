package com.example.asus.infokosmalang.MainActivity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.asus.infokosmalang.Login;
import com.example.asus.infokosmalang.Pop;
import com.example.asus.infokosmalang.R;
import com.example.asus.infokosmalang.tamu.NavigasiHomeTamu;

public class MainActivity extends AppCompatActivity {

    private long lastPressedTime;
    private static final int PERIOD = 2000;


    Button btn_tamu, btn_pemilik;
    ImageButton btn_info;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_tamu = (Button) findViewById(R.id.btn_tamu);
        btn_pemilik = (Button) findViewById(R.id.btn_pemilik);
        btn_info = (ImageButton) findViewById(R.id.btn_info);


        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
// TODO Auto-generated method stub
         startActivity(new Intent(MainActivity.this, Pop.class));
            }
        });

        btn_tamu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
// TODO Auto-generated method stub
                Intent inte = new Intent(MainActivity.this, NavigasiHomeTamu.class);
                startActivity(inte);
            }
        });

        btn_pemilik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
// TODO Auto-generated method stub
                Intent inte = new Intent(MainActivity.this, Login.class);
                startActivity(inte);
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (event.getDownTime() - lastPressedTime < PERIOD) {
                        moveTaskToBack(true);
                    } else {
                        Toast.makeText(getApplicationContext(), "Tekan sekali lagi untuk keluar dari Aplikasi.",
                                Toast.LENGTH_SHORT).show();
                        lastPressedTime = event.getEventTime();
                    }
                    return true;
            }
        }
        return false;
    }

}