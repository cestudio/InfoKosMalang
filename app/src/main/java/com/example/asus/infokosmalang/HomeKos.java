package com.example.asus.infokosmalang;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Asus on 5/30/2017.
 */

public class HomeKos extends Fragment {

    public static String FEED_URL;

    private static final String TAG = NavigasiHome.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterListKos mGridAdapter;
    private ArrayList<GridItemListKos> mGridData;
    private static final String TAG_IDKOS = "idkos";
    private static final String TAG_NAMAKOS = "namakos";
    private static final String TAG_ALAMATKOS = "alamatkos";
    private static final String TAG_HARGAPERBULAN = "hargaperbulan";
    private static final String TAG_HARGAPERTAHUN = "hargapertahun";
    private static final String TAG_GAMBARKOS = "gambarkos";

    static boolean a=false;

    JSONArray rs = null;

    ProgressDialog pd;

    public HomeKos(){
        //required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.homekos, container, false);

        FEED_URL = Config.LISTKOS_URL;
        FEED_URL += "?kode=home";

        Log.e("List Kos = ", FEED_URL);

        mGridView = (GridView) rootview.findViewById(R.id.gridView);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterListKos(getActivity(), R.layout.grid_item_list_kos, mGridData);
        mGridView.setAdapter(mGridAdapter);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Sedang mengambil data, tunggu sebentar...");
        pd.setCancelable(false);
        pd.show();

        //Start download
        new AsyncHttpTask().execute(FEED_URL);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                //Get item at position
                GridItemListKos item = (GridItemListKos) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor.putString("idkos", item.getIdkos());
                editor.commit();

                Intent intent = new Intent(getActivity(), DetailKos.class);
                startActivity(intent);

            }
        });

        return rootview;

    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);

                pd.hide();
            } else {
//                Toast.makeText(getActivity(), "Failed to fetch data!", Toast.LENGTH_SHORT).show();
                pd.hide();
            }


        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemListKos item;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String stridkos = post.getString(TAG_IDKOS);
//                String strnamakos = post.getString(TAG_NAMAKOS);
                String stralamatkos = post.getString(TAG_ALAMATKOS);
                String strhargaperbulan = post.getString(TAG_HARGAPERBULAN);
                String strhargapertahun = post.getString(TAG_HARGAPERTAHUN);
                String strgambarkos = post.getString(TAG_GAMBARKOS);
                item = new GridItemListKos();
                item.setIdkos(stridkos);
//                item.setNamakos(strnamakos);
                item.setAlamatkos(stralamatkos);
                item.setHargaperbulan("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargaperbulan))).replace(",","."));
                item.setHargapertahun("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(strhargapertahun))).replace(",","."));
                item.setGambarkos(strgambarkos);

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
