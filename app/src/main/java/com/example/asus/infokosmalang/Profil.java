package com.example.asus.infokosmalang;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Asus on 5/30/2017.
 */

public class Profil extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.profil,container, false);
        TextView email = (TextView) rootview.findViewById(R.id.tv_email2);
        email.setText(getActivity().getSharedPreferences("tech", Context.MODE_PRIVATE).getString("email",""));
        return rootview;
    }
 

}
