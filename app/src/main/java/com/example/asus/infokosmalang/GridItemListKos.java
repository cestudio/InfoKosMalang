package com.example.asus.infokosmalang;

public class GridItemListKos {

    String idkos,namakos,alamatkos,hargaperbulan,hargapertahun,gambarkos;

    public String getIdkos() {
        return idkos;
    }

    public void setIdkos(String idkos) {
        this.idkos = idkos;
    }

    public String getNamakos() {
        return namakos;
    }

    public void setNamakos(String namakos) {
        this.namakos = namakos;
    }

    public String getAlamatkos() {
        return alamatkos;
    }

    public void setAlamatkos(String alamatkos) {
        this.alamatkos = alamatkos;
    }

    public String getHargaperbulan() {
        return hargaperbulan;
    }

    public void setHargaperbulan(String hargaperbulan) {
        this.hargaperbulan = hargaperbulan;
    }

    public String getHargapertahun() {
        return hargapertahun;
    }

    public void setHargapertahun(String hargapertahun) {
        this.hargapertahun = hargapertahun;
    }

    public String getGambarkos() {
        return gambarkos;
    }

    public void setGambarkos(String gambarkos) {
        this.gambarkos = gambarkos;
    }

}