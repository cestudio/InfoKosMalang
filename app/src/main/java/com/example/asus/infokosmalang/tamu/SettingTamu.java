package com.example.asus.infokosmalang.tamu;

/**
 * Created by Decky Ilham S on 6/8/2017.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.example.asus.infokosmalang.R;

/**
 * Created by Asus on 5/30/2017.
 */

public class SettingTamu extends Fragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*Add in Oncreate() funtion after setContentView()*/
        Switch simpleSwitch = (Switch) view.findViewById(R.id.s_notif); // initiate Switch

        simpleSwitch.setTextOn("On"); // displayed text of the Switch whenever it is in checked or on state
        simpleSwitch.setTextOff("Off"); // displayed text of the Switch whenever it is in unchecked i.e. off state


        getActivity().setTitle("Pengaturan");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settingtamu,container, false);
    }


}
