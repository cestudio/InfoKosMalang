package com.example.asus.infokosmalang;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapterListKos extends ArrayAdapter<GridItemListKos> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemListKos> mGridData = new ArrayList<GridItemListKos>();

    public GridViewAdapterListKos(Context mContext, int layoutResourceId, ArrayList<GridItemListKos> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemListKos> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idkos = (TextView) row.findViewById(R.id.idkos);
//            holder.namakos = (TextView) row.findViewById(R.id.namakos);
            holder.alamatkos = (TextView) row.findViewById(R.id.alamatkos);
            holder.hargapertahun = (TextView) row.findViewById(R.id.hargapertahun);
            holder.hargaperbulan = (TextView) row.findViewById(R.id.hargaperbulan);
            holder.gambarkos = (ImageView) row.findViewById(R.id.gambarkos);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemListKos item = mGridData.get(position);
        holder.idkos.setText(Html.fromHtml(item.getIdkos()));
//        holder.namakos.setText(Html.fromHtml(item.getNamakos()));
        holder.alamatkos.setText(Html.fromHtml(item.getAlamatkos()));
        holder.hargapertahun.setText(Html.fromHtml(item.getHargapertahun()));
        holder.hargaperbulan.setText(Html.fromHtml(item.getHargaperbulan()));
        byte[] decodedString = Base64.decode(item.getGambarkos(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        holder.gambarkos.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, 300, 300, false));

        return row;
    }

    static class ViewHolder {
        TextView idkos,namakos,alamatkos,hargapertahun,hargaperbulan;
        ImageView gambarkos;
    }
}