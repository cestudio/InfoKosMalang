package com.example.asus.infokosmalang;

/**
 * Created by Asus on 6/8/2017.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.asus.infokosmalang.MainActivity.MainActivity;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Thread th = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(getSharedPreferences("DATA",MODE_PRIVATE).getString("statlogin","").equals("1")){
                        Intent intent = new Intent(SplashScreen.this, NavigasiHome.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }

        };
        th.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}