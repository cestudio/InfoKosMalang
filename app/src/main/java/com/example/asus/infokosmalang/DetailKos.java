package com.example.asus.infokosmalang;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.asus.infokosmalang.MainActivity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DetailKos extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    SliderLayout sliderLayout;
    HashMap<String,String> Hash_file_maps ;

    public static String FEED_URL;

    private static final String TAG_RESULT = "result";
    private static final String TAG_GAMBAR1 = "gambar1";
    private static final String TAG_GAMBAR2 = "gambar2";
    private static final String TAG_GAMBAR3 = "gambar3";
    private static final String TAG_GAMBAR4 = "gambar4";

    private static final String TAG_ALAMATKOS = "alamatkos";
    private static final String TAG_KELURAHAN = "kelurahan";
    private static final String TAG_KECAMATAN = "kecamatan";
    private static final String TAG_KOTA = "kota";
    private static final String TAG_PROVINSI = "provinsi";
    private static final String TAG_FASILITAS = "fasilitas";
    private static final String TAG_BIAYAPERTAHUN = "hargapertahun";
    private static final String TAG_BIAYAPERBULAN = "hargaperbulan";
    private static final String TAG_AREA = "area";

    static boolean a=false;

    JSONArray rs = null;

    TextView alamatkos,fasilitas,biayapertahun,biayaperbulan,area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Hash_file_maps = new HashMap<String, String>();

        sliderLayout = (SliderLayout)findViewById(R.id.slider);
        alamatkos = (TextView) findViewById(R.id.alamatkos);
        fasilitas = (TextView) findViewById(R.id.fasilitas);
        biayaperbulan = (TextView) findViewById(R.id.hargaperbulan);
        biayapertahun = (TextView) findViewById(R.id.hargapertahun);
        area = (TextView) findViewById(R.id.area);

        new detailKosan().execute();
    }
    @Override
    protected void onStop() {

        sliderLayout.stopAutoCycle();

        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {

        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    private class detailKosan extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.LISTKOS_URL;
            FEED_URL += "?idkos="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idkos","");
            FEED_URL += "&kode=detail";
            Log.e("Detail Kos = ",FEED_URL);
            String URL = FEED_URL;

            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strgambar1 = a.getString(TAG_GAMBAR1);
                        String strgambar2 = a.getString(TAG_GAMBAR2);
                        String strgambar3 = a.getString(TAG_GAMBAR3);
                        String strgambar4 = a.getString(TAG_GAMBAR4);

                        String stralamatkos = a.getString(TAG_ALAMATKOS);
                        String strkelurahan = a.getString(TAG_KELURAHAN);
                        String strkecamatan = a.getString(TAG_KECAMATAN);
                        String strkota = a.getString(TAG_KOTA);
                        String strprovinsi = a.getString(TAG_PROVINSI);
                        String strfasilitas = a.getString(TAG_FASILITAS);
                        String strbiayapertahun = a.getString(TAG_BIAYAPERTAHUN);
                        String strbiayaperbulan = a.getString(TAG_BIAYAPERBULAN);
                        String strarea = a.getString(TAG_AREA);

                        alamatkos.setText(stralamatkos+", "+strkelurahan+", "+strkecamatan+", "+strkota+", "+strprovinsi);
                        fasilitas.setText(strfasilitas);
                        biayaperbulan.setText(strbiayaperbulan);
                        biayapertahun.setText(strbiayapertahun);
                        area.setText(strarea);

                        Hash_file_maps.put("Gambar 1", strgambar1);
                        Hash_file_maps.put("Gambar 2", strgambar2);
                        Hash_file_maps.put("Gambar 3", strgambar3);
                        Hash_file_maps.put("Gambar 4", strgambar4);
                    }


                    for(String name : Hash_file_maps.keySet()){

                        TextSliderView textSliderView = new TextSliderView(DetailKos.this);
                        textSliderView
                                .description(name)
                                .image(Hash_file_maps.get(name))
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .setOnSliderClickListener(DetailKos.this);
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle()
                                .putString("extra",name);
                        sliderLayout.addSlider(textSliderView);
                    }
                    sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                    sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    sliderLayout.setCustomAnimation(new DescriptionAnimation());
                    sliderLayout.setDuration(3000);
                    sliderLayout.addOnPageChangeListener(DetailKos.this);

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}