package com.example.asus.infokosmalang;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

/**
 * Created by Asus on 5/30/2017.
 */

public class Setting extends Fragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*Add in Oncreate() funtion after setContentView()*/
        Switch simpleSwitch = (Switch) view.findViewById(R.id.s_notif); // initiate Switch
        Switch simpleSwitch2 = (Switch) view.findViewById(R.id.s_login); //nitiate Switch 2

        simpleSwitch.setTextOn("On"); // displayed text of the Switch whenever it is in checked or on state
        simpleSwitch.setTextOff("Off"); // displayed text of the Switch whenever it is in unchecked i.e. off state

        simpleSwitch2.setTextOn("On"); // displayed text of the Switch whenever it is in checked or on state
        simpleSwitch2.setTextOff("Off"); // displayed text of the Switch whenever it is in unchecked i.e. off state
        getActivity().setTitle("Pengaturan");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setting,container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Button button = (Button) getActivity().findViewById(R.id.btn_ubahprofil);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ngintent();

            }
        });
        super.onActivityCreated(savedInstanceState);
    }

    private void ngintent() {


    }
}
