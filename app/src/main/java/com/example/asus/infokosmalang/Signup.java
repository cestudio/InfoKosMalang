package com.example.asus.infokosmalang;

/**
 * Created by Decky Ilham Syahputra 6/8/2017.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Signup extends AppCompatActivity {
    EditText edit_username;
    EditText edit_email;
    EditText edit_pass;
    Button btn_sign;
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        edit_username = (EditText) findViewById(R.id.id_nama);
        edit_email = (EditText) findViewById(R.id.id_email);
        edit_pass = (EditText) findViewById(R.id.id_katasandi);
        btn_sign = (Button) findViewById(R.id.btn_daftar);



        btn_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        btn_login=(Button)findViewById(R.id.btn_masuk);


        final EditText ed = (EditText) findViewById(R.id.id_katasandi);
        CheckBox c = (CheckBox) findViewById(R.id.checkBox1);

        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (!isChecked) {
                    ed.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    ed.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin=new Intent(Signup.this,Login.class);
                startActivity(intentLogin);
                finish();
            }
        });
    }

    private void registerUser() {
        String username = edit_username.getText().toString().trim().toLowerCase();
        String email = edit_email.getText().toString().trim().toLowerCase();
        String password = edit_pass.getText().toString().trim().toLowerCase();
        register(username, password, email);
    }


    private void register(String username, String password, String email){

        if (TextUtils.isEmpty(username)) {
            Toast.makeText(getApplicationContext(), "Masukan Nama !", Toast.LENGTH_SHORT).show();
            return;
        }


        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Masukan Email!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Masukan Kata Sandi!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "Kata Sandi Minimal terdiri dari 6 Karakter", Toast.LENGTH_SHORT).show();
            return;
        }



        Log.wtf("username", username);
        Log.wtf("email", email);
        Log.wtf("password", password);




        String urlSuffix = "?username=" + username + "&password=" + password + "&email=" + email;
        class RegisterUser extends AsyncTask<String, Void, String> {


            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Signup.this, "Proses Daftar...", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                edit_username.setText("");
                edit_email.setText("");
                edit_pass.setText("");

                if(s.equals("0")){
                    Toast.makeText(Signup.this, "Pendaftaran Gagal", Toast.LENGTH_SHORT).show();
                }
                else if(s.equals("2")){
                    Toast.makeText(Signup.this, "Username telah digunakan", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intentLogin=new Intent(Signup.this,Login.class);
                    startActivity(intentLogin);
                    Toast.makeText(getApplicationContext(),"Sukses", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferReader=null;
                try {
                    URL url=new URL(Config.REGISTER_URL+s);
                    HttpURLConnection con=(HttpURLConnection)url.openConnection();
                    bufferReader=new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String result;
                    result=bufferReader.readLine();
                    return  result;

                }catch (Exception e){
                    return null;
                }
            }

        }
        RegisterUser ur=new RegisterUser();
        ur.execute(urlSuffix);
    }
}