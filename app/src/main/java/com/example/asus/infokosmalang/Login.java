package com.example.asus.infokosmalang;

/**
 * Created by Asus on 6/8/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    public static String FEED_URL;

    ImageView logo;

    private static final String TAG_RESULT = "result";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMAUSER = "namauser";
    private static final String TAG_EMAILUSER = "emailuser";

    static boolean a=false;

    JSONArray rs = null;

    private EditText editTextEmail, editTextPassword;
    private String email, katasandi;
    private Button BtnLogin;
    private boolean loggedIn = false;
    private ProgressDialog dialog;
    private Button BtnSignup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editTextEmail = (EditText) findViewById(R.id.id_email);
        editTextPassword = (EditText) findViewById(R.id.id_katasandi);
        BtnLogin = (Button) findViewById(R.id.btn_masuk);
        BtnSignup = (Button) findViewById(R.id.btn_daftar);


        final EditText ed = (EditText) findViewById(R.id.id_katasandi);
        CheckBox c = (CheckBox) findViewById(R.id.checkBox1);

        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                if (!isChecked) {
                    ed.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    ed.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });


        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new login().execute();
            }
        });
        BtnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin = new Intent(Login.this, Signup.class);
                startActivity(intentLogin);
                finish();
            }
        });
    }


    private class login extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = Config.LOGIN_URL;
            FEED_URL += "?username="+editTextEmail.getText().toString();
            FEED_URL += "&password="+editTextPassword.getText().toString();
            Log.e("Login = ",FEED_URL);
            String URL = FEED_URL;

            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String login = a.getString(TAG_LOGIN);

                        if (login.equals("0")){
                            Toast.makeText(Login.this, "Username/Password Anda Salah!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String iduser = a.getString(TAG_IDUSER);
                            String namauser = a.getString(TAG_NAMAUSER);
                            String emailuser = a.getString(TAG_EMAILUSER);

                            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("namauser", namauser);
                            editor.putString("emailuser", emailuser);
                            editor.commit();
                            startActivity(new Intent(Login.this,NavigasiHome.class));
                            finish();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }


}

