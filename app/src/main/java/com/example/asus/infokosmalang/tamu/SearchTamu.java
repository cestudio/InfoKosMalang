package com.example.asus.infokosmalang.tamu;

/**
 * Created by Decky Ilham S on 6/8/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.asus.infokosmalang.R;

/**
 * Created by Asus on 6/6/2017.
 */

public class SearchTamu extends AppCompatActivity {

    Button btn_search ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tamu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(com.example.asus.infokosmalang.tamu.SearchTamu.this, NavigasiHomeTamu.class);
                startActivity(intent);
            }
        });

    }
}
